Name:           perl-Text-Tabs+Wrap
Version:        2024.001
Release:        2
Summary:        Expand tabs and do simple line wrapping
License:        TTWL
URL:            https://metacpan.org/release/Text-Tabs%2BWrap
Source0:        https://cpan.metacpan.org/authors/id/A/AR/ARISTOTLE/Text-Tabs+Wrap-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  perl-interpreter perl-generators
BuildRequires:  perl(ExtUtils::MakeMaker) perl(Test::More)
Conflicts:      perl < 4:5.20.2-325

%description
Text::Tabs does most of what the unix utilities expand(1) and unexpand(1) do. Given
a line with tabs in it, expand replaces those tabs with the appropriate number of
spaces. Given a line with or without tabs in it, unexpand adds tabs when it can save
bytes by doing so, like the unexpand -a command.

Unlike the old unix utilities, this module correctly accounts for any Unicode combining
characters (such as diacriticals) that may occur in each line for both expansion and
unexpansion. These are overstrike characters that do not increment the logical position.
Make sure you have the appropriate Unicode settings enabled.

%package_help

%prep
%autosetup -n Text-Tabs+Wrap-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} %{buildroot}/*

%check
make test

%files
%{perl_vendorlib}/*

%files help
%doc Changes README
%{_mandir}/man3/*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 2024.001-2
- drop useless perl(:MODULE_COMPAT) requirement

* Fri Aug 16 2024 wuzhaomin <wuzhaomin@kylinos.cn> - 2024.001-1
- update version to 2024.001
- Cleaned up, reformatted, backfilled Changes

* Thu Jul 20 2023 xujing <xujing125@huawei.com> - 2023.0511-1
- update version to 2023.0511

* Mon Oct 24 2022 xujing <xujing125@huawei.com> - 2021.0814-2
- optimize the specfile

* Fri Dec  3 2021 guozhaorui <guozhaorui1@huawei.com> - 2021.0814-1
- update version to 2021.0814

* Sun Jan 12 2020 openEuler Buildteam <buildteam@openeuler.org> - 2013.0523-419
- Delete redundant file

* Mon Sep 16 2019 luhuaxin <luhuaxin@huawei.com> - 2013.0523-418
- Package init
